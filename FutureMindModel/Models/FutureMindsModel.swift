//
//  FutureMindsModel.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 01/02/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation
import CoreData

@objc(FutureMindModel)
class FutureMindModel: NSManagedObject, Codable {
    private enum CodingKeys: String, CodingKey {
        case imageURL = "image_url"
        case desc = "description"
        case modificationDate, orderId, title
    }
    
    @NSManaged var desc: String
    @NSManaged var imageURL: String
    @NSManaged var modificationDate: Date
    @NSManaged var orderId: Int
    @NSManaged var title: String
    
    @NSManaged var data: Data?
    
    required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "FutureMindModel", in: managedObjectContext) else {
                fatalError("Failed to decode User")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)

        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        desc = try container.decode(String.self, forKey: .desc)
        imageURL = try container.decode(String.self, forKey: .imageURL)
        
        let modificationDateString = try container.decode(String.self, forKey: .modificationDate)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        modificationDate = formatter.date(from: modificationDateString) ?? Date()
        
        orderId = try container.decode(Int.self, forKey: .orderId)
        title = try container.decode(String.self, forKey: .title)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(desc, forKey: .desc)
        try container.encode(imageURL, forKey: .imageURL)
        try container.encode(modificationDate, forKey: .modificationDate)
        try container.encode(orderId, forKey: .orderId)
        try container.encode(title, forKey: .title)
    }
}

extension CodingUserInfoKey {
    // Helper property to retrieve the context
    static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")
}

extension NSManagedObject {
    static var entityName: String {
        return "\(self)"
    }
}
