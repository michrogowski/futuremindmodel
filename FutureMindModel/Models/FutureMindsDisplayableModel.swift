//
//  FutureMindsDisplayableModel.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 03/02/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation
import UIKit

struct FutureMindsDisplayableModel {
    
    let avatar: UIImage
    let modelDescription: String
    let title: String
    let modificationDate: String
    let orderId: Int
    let detailsURL: URL?
    
    init(model: FutureMindModel) {
        if let data = model.data, let image = UIImage(data: data) {
            avatar = image
        } else {
            avatar = UIImage(named: "avatar_placeholder") ?? UIImage()
        }
        
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches = detector.matches(in: model.desc, options: .reportCompletion, range: NSRange(location: 0, length: model.desc.utf8.count))
                .sorted { $0.range.location < $1.range.location }
            
            detailsURL = matches.last?.url
            modelDescription = String(model.desc.dropLast(matches.last?.url?.absoluteString.count ?? 0))
        } catch let error as NSError {
            //TODO: Handle error
            print("erorr when creating data detector \(error)")
            
            modelDescription = model.desc
            detailsURL = nil
        }
        
        title = model.title
        orderId = model.orderId
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        modificationDate = formatter.string(from: model.modificationDate)
    }
}
