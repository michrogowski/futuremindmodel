//
//  AppDelegate.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 01/02/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = FutureMindsResultsFlowCoordinator.prepareView(with: nil)
        window?.makeKeyAndVisible()
        
        return true
    }
}

