//
//  Localizable.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 31/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation

enum Localizable {

    static let title = "title"

    static let description = "description"

    static let created_at = "created_at"
    
    static let main_results = "main_results"
    
    static let details = "details"
}

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
