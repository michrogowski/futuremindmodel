//
//  UserDefaults+Values.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation

private enum UserDefaultsKey: String {
    case ratedRepositories = "repositories.ratedValue"
}

extension UserDefaults {
    
    var ratedRepositories: [String: Bool] {
        
        get {
            return dictionary(forKey: UserDefaultsKey.ratedRepositories.rawValue) as? [String: Bool] ?? [:]
        }
        set {
            set(newValue, forKey: UserDefaultsKey.ratedRepositories.rawValue)
        }
    }
}
