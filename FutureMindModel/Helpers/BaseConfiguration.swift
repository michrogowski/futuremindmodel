//
//  BaseConfiguration.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation

struct BaseConfiguration {
    static var baseURL: URL {
        let stringURL = "https://www.futuremind.com/"
        
        guard let requestURL = URL(string: stringURL) else {
            fatalError("Can't create URL from base URL")
        }
        
        return requestURL
    }
}
