//
//  WebViewDetailsViewModel.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 03/02/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation

final class WebViewDetailsViewModel: ViewModel {
    
    typealias Coordinator = WebViewDetailsFlowCoordinator

    var flowCoordinator: WebViewDetailsFlowCoordinator?
    
    var url: URL?
    
    init(flowCoordinator: WebViewDetailsFlowCoordinator) {
        self.flowCoordinator = flowCoordinator
    }
}
