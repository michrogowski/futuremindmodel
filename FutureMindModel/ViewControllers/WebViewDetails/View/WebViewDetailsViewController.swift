//
//  WebViewDetailsViewController.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 03/02/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit

final class WebViewDetailsViewController: BaseViewController<WebViewDetailsView, WebViewDetailsViewModel> {
        
    override func viewDidLoad() {
        super.viewDidLoad()

        title = Localizable.details.localized
        
        guard let url = viewModel?.url else { return }
        baseView?.urlToShow.onNext(url)
    }
}
