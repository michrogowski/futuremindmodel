//
//  WebViewDetailsView.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 03/02/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import WebKit

final class WebViewDetailsView: UIView {
    
    let urlToShow = PublishSubject<URL>()
    
    private let webView = WKWebView()
    
    private let disposeBag = DisposeBag()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupBinding()
    }
    
    private func setupViews() {
        backgroundColor = .white
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(webView)
        
        let anchors = [webView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
                       webView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
                       webView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
                       webView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor)]
        
        NSLayoutConstraint.activate(anchors)
    }
        
    private func setupBinding() {
        
        urlToShow
            .take(1)
            .asDriver(onErrorDriveWith: .never())
            .drive(onNext: { [weak self] url in
                self?.webView.load(URLRequest(url: url))
            })
            .disposed(by: disposeBag)
    }

}
