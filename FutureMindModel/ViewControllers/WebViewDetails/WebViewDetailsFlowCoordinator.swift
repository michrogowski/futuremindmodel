//
//  WebViewDetailsFlowCoordinator.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 03/02/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit

final class WebViewDetailsFlowCoordinator {
    
    typealias Navigator = UIViewController
    
    weak var navigator: UIViewController?
}

extension WebViewDetailsFlowCoordinator: FlowCoordinator {
    
    static func prepareView(with navigator: UIViewController?) -> UIViewController {
        
        let flowCoordinator = WebViewDetailsFlowCoordinator()
        let viewController = WebViewDetailsViewController()
        let viewModel = WebViewDetailsViewModel(flowCoordinator: flowCoordinator)
        
        viewController.viewModel = viewModel
        
        return viewController
    }
}
