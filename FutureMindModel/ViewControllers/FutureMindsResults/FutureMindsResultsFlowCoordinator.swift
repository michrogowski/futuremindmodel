//
//  FutureMindsResultsFlowCoordinator.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CoreData

final class FutureMindsResultsFlowCoordinator {
    
    weak var navigator: UINavigationController?
    
    private let networkManager = NetworkManager()
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "FutureMindModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                //TODO: ADD ERROR HANDLING
                assertionFailure("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    required init() {}
    
    func goToDetails(for model: FutureMindsDisplayableModel) {

        let controller = WebViewDetailsFlowCoordinator.prepareView(with: navigator?.viewControllers.last)
        (controller as? WebViewDetailsViewController)?.viewModel?.url = model.detailsURL
        navigator?.pushViewController(controller, animated: true)
    }
    
}

extension FutureMindsResultsFlowCoordinator: FlowCoordinator {
    static func prepareView(with navigator: UINavigationController?) -> UIViewController {
        
        let flowCoordinator = FutureMindsResultsFlowCoordinator()
        let futureMindsResultsViewController = FutureMindsResultsViewController()
        let viewModel = FutureMindsResultsViewModel(flowCoordinator: flowCoordinator)
        viewModel.persistentContainer = flowCoordinator.persistentContainer
        futureMindsResultsViewController.viewModel = viewModel
        
        viewModel.networkManager = flowCoordinator.networkManager
        let navigatorController = UINavigationController(rootViewController: futureMindsResultsViewController)
        flowCoordinator.navigator = navigatorController
        return navigatorController
    }
}
