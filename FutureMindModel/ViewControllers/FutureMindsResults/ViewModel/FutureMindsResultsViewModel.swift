//
//  FutureMindsResultsViewModel.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreData

//FIXME: You have misspell in your json return, instead of http://lorempixel.com/120/120/annimals/ it should be http://lorempixel.com/120/120/animals/ :) That's why some images are black

final class FutureMindsResultsViewModel: ViewModel {
    typealias Coordinator = FutureMindsResultsFlowCoordinator
    
    var flowCoordinator: FutureMindsResultsFlowCoordinator?
    
    var reload: PublishSubject<IndexPath?> = PublishSubject()
    
    var futureMindsResultsEvents: PublishSubject<FutureMindsResultsListEventReceiver> = PublishSubject()
    
    weak var networkManager: NetworkManager?
    
    private weak var searchRepositoriesEventReceiver: FutureMindsResultsListEventReceiver?
    
    private var data: [FutureMindModel] = [] {
        didSet {
            updateImagesIfNeeded()
        }
    }
    
    private let disposeBag = DisposeBag()
    
    private var query: NetworkQuery = FutureMindQuery()
    
    var persistentContainer: NSPersistentContainer?

    required init(flowCoordinator: FutureMindsResultsFlowCoordinator) {
        self.flowCoordinator = flowCoordinator
        
        setupBinding()
    }
    
    private func setupBinding() {
        let shareEvents = futureMindsResultsEvents.share()
        
        shareEvents
            .flatMap { $0.selectEvent }
            .subscribe(onNext: { [weak self] indexPath in
                guard let element = self?.data[indexPath.row] else { return }
                self?.flowCoordinator?.goToDetails(for: FutureMindsDisplayableModel(model: element))
            })
            .disposed(by: disposeBag)
        
        shareEvents
            .flatMap { $0.refreshEvent }
            .subscribe(onNext: { [weak self] _ in
                self?.clearStorage()
                self?.sendQueryIfNeeded()
            })
            .disposed(by: disposeBag)
        
        shareEvents
            .subscribe(onNext: { [weak self] _ in
                self?.sendQueryIfNeeded()
            })
            .disposed(by: disposeBag)
    }
    
    private func sendQueryIfNeeded() {
        let storageObjects = storageData
        
        guard storageObjects.isEmpty else {
            data = storageData
            reload.onNext(nil)
            return
        }
        
        networkManager?
            .send(query: query, completion: { [weak self] result in
                self?.handle(result: result, isFetchingMore: false)
            })
    }
    
    private func handle(result: NetworkResult, isFetchingMore: Bool) {
        guard result.error == nil else {
            //TODO: ADD SOMETHING WENT WRONG ALERT FOR A USER AND RETRY IF NEEDED
            return
        }

        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = persistentContainer?.viewContext else {
            //TODO: ADD BAD CONFIGURATION HANDLER
            assertionFailure("Failed to retrieve context")
            return
        }
        
        do {
            let decoder = JSONDecoder()
            decoder.userInfo[codingUserInfoKeyManagedObjectContext] = managedObjectContext
            data = try result.result([FutureMindModel].self, decoder: decoder).sorted { $0.orderId < $1.orderId }
            reload.onNext(nil)
            
            guard managedObjectContext.hasChanges else { return }
            try managedObjectContext.save()
        } catch let error as NSError {
            data = storageData
            reload.onNext(nil)
            
            print("error couldn't map object \(error) \(String(describing: result.urlResponse?.url?.absoluteString))")
            //TODO: Handle error
        }
        
    }
    
    func clearStorage() {
        guard let managedObjectContext = persistentContainer?.viewContext else { return }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: FutureMindModel.entityName)
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try managedObjectContext.execute(batchDeleteRequest)
        } catch let error as NSError {
            print(error)
            //TODO: Handle error
        }
    }
    
    var storageData: [FutureMindModel] {
        guard let managedObjectContext = persistentContainer?.viewContext else { return [] }
        
        let fetchRequest = NSFetchRequest<FutureMindModel>(entityName: FutureMindModel.entityName)
        
        let sortDescriptor1 = NSSortDescriptor(key: "orderId", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor1]
        
        do {
            let users = try managedObjectContext.fetch(fetchRequest)
            return users
        } catch let error {
            print("Error occured while fetching models from core data \(error.localizedDescription)")
            //TODO: Handle error
            return []
        }
    }
    
    private func updateImagesIfNeeded() {
        for (index, model) in data.enumerated() {
            guard model.data == nil else { continue }
            self.downloadImage(for: model, at: index)
        }
    }
    
    private func downloadImage(for model: FutureMindModel, at index: Int) {
        networkManager?.send(query: ImageQuery(url: model.imageURL), completion: { [weak self] result in
            guard result.error == nil else {
                //TODO: Handle error
                return
            }
            
            guard self?.data.count ?? 0 > index && self?.data[index] == model else { return }
            model.data = result.data
            do {
                try self?.persistentContainer?.viewContext.save()
                self?.reload.onNext(IndexPath(row: index, section: 0))
            } catch let error as NSError {
                print("Error while saving image \(error)")
                //TODO: Handle error
            }
        })
    }
}

extension FutureMindsResultsViewModel: FutureMindsResultsDataProvider {
    
    var numberOfItems: Int {
        return data.count
    }
    
    func element(for indexPath: IndexPath) -> FutureMindsDisplayableModel {
        return FutureMindsDisplayableModel(model: data[indexPath.row])
    }
    
}
