//
//  FutureMindsResultsViewController.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit

final class FutureMindsResultsViewController: BaseViewController<FutureMindsResultsView, FutureMindsResultsViewModel> {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Localizable.main_results.localized
        
        setupBinding()
    }
    
    private func setupBinding() {
        guard let view = baseView else { return }
        
        if let provider = viewModel {
            view.providerSubject.onNext(provider)
        }
        
        viewModel?.futureMindsResultsEvents.onNext(view)
    }
}
