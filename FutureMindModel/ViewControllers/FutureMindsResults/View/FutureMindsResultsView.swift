//
//  FutureMindsResultsView.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol FutureMindsResultsDataProvider: class {
    var reload: PublishSubject<IndexPath?> { get }
    
    var numberOfItems: Int { get }
    
    func element(for indexPath: IndexPath) -> FutureMindsDisplayableModel
}

protocol FutureMindsResultsListEventReceiver: class {
    var selectEvent: PublishSubject<IndexPath> { get }
    
    var refreshEvent: ControlEvent<Void> { get }
}

final class FutureMindsResultsView: UIView, FutureMindsResultsListEventReceiver {

    var providerSubject: PublishSubject<FutureMindsResultsDataProvider> = PublishSubject()
    
    var selectEvent: PublishSubject<IndexPath> = PublishSubject()

    var refreshEvent: ControlEvent<Void> {
        return refreshControl.rx
            .controlEvent(.valueChanged)
    }
    
    weak var provider: FutureMindsResultsDataProvider? {
        didSet {
            setupProviderBinding()
        }
    }
    
    private let tableView = UITableView()
    private let indicator = UIActivityIndicatorView()
    private let refreshControl = UIRefreshControl()
    
    let disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupBinding()
    }
    
    private func setupViews() {
        backgroundColor = .white
        setupTableView()
        setupActivityIndicator()
        
        let anchors = [tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
                       tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
                       tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
                       tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
                       indicator.centerXAnchor.constraint(equalTo: centerXAnchor),
                       indicator.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor)]
        
        NSLayoutConstraint.activate(anchors)
    }
    
    private func setupTableView() {
        refreshControl.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(FutureMindsResultsTableViewCell.self, forCellReuseIdentifier: "\(FutureMindsResultsTableViewCell.self)")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = refreshControl
        
        addSubview(tableView)
    }
    
    private func setupActivityIndicator() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.style = .whiteLarge
        indicator.hidesWhenStopped = true
        indicator.backgroundColor = .lightGray
        indicator.layer.cornerRadius = 5.0
        
        addSubview(indicator)
    }
    
    private func setupBinding() {
        providerSubject
            .take(1)
            .subscribe(onNext: { [weak self] dataProvider in
                self?.provider = dataProvider
            })
            .disposed(by: disposeBag)
        
        tableView.rx
            .itemSelected
            .bind(to: selectEvent)
            .disposed(by: disposeBag)
    }
    
    private func setupProviderBinding() {
        provider?
            .reload
            .asDriver(onErrorJustReturn: nil)
            .drive(onNext: { [weak self] indexPath in
                self?.reloadData(for: indexPath)
                
            })
            .disposed(by: disposeBag)
        
        provider?
            .reload
            .asDriver(onErrorJustReturn: nil)
            .map { _ in false }
            .drive(refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)
        
        provider?
            .reload
            .asDriver(onErrorJustReturn: nil)
            .map { _ in false }
            .drive(indicator.rx.isAnimating)
            .disposed(by: disposeBag)

    }
    
    private func reloadData(for indexPath: IndexPath?) {
        if let indexPath = indexPath {
            tableView.reloadRows(at: [indexPath], with: .none)
        } else {
            tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .automatic)
            tableView.tableFooterView = nil
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FutureMindsResultsView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return provider?.numberOfItems ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let element = provider?.element(for: indexPath)
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(FutureMindsResultsTableViewCell.self)", for: indexPath) as? FutureMindsResultsTableViewCell else {
            assertionFailure("Can't create cell")
            
            return UITableViewCell()
        }
        
        guard let rowElement = element else { return cell }
        
        cell.setup(with: rowElement)
        
        return cell
    }
}
