//
//  FutureMindsResultsResultTableViewCell.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit

final class FutureMindsResultsTableViewCell: UITableViewCell {

    private let titleLabel = UILabel()
    private let descLabel = UILabel()
    private let modificationDateLabel = UILabel()
    private let avatarImageView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "\(FutureMindsResultsTableViewCell.self)")
        setupViews()
        setupFonts()
    }
    
    private func setupViews() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        descLabel.translatesAutoresizingMaskIntoConstraints = false
        modificationDateLabel.translatesAutoresizingMaskIntoConstraints = false
        avatarImageView.translatesAutoresizingMaskIntoConstraints = false
        
        let verticalStackView = UIStackView(arrangedSubviews: [titleLabel, descLabel, modificationDateLabel])
        verticalStackView.axis = .vertical
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        
        avatarImageView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        avatarImageView.contentMode = .scaleAspectFit
        contentView.addSubview(verticalStackView)
        contentView.addSubview(avatarImageView)

        let anchors = [avatarImageView.heightAnchor.constraint(equalToConstant: 50),
                       avatarImageView.heightAnchor.constraint(equalTo: avatarImageView.widthAnchor),
                       avatarImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 5),
                       verticalStackView.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: 5),
                       avatarImageView.centerYAnchor.constraint(equalTo: verticalStackView.centerYAnchor),
                       verticalStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
                       contentView.trailingAnchor.constraint(equalTo: verticalStackView.trailingAnchor, constant: 5),
                       contentView.bottomAnchor.constraint(equalTo: verticalStackView.bottomAnchor, constant: 5)]
        
        NSLayoutConstraint.activate(anchors)
    }
    
    private func setupFonts() {
        titleLabel.font = .boldSystemFont(ofSize: 25)
        descLabel.font = .systemFont(ofSize: 14)
        descLabel.numberOfLines = 0
        modificationDateLabel.font = .boldSystemFont(ofSize: 14)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(with element: FutureMindsDisplayableModel) {
        titleLabel.text = Localizable.title.localized + element.title
        descLabel.text = Localizable.description.localized + element.modelDescription
        modificationDateLabel.text = Localizable.created_at.localized + "\(element.modificationDate)"
        avatarImageView.image = element.avatar
    }
}
