//
//  BaseViewController.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit

class BaseViewController<T: UIView, Y: ViewModel>: UIViewController, MVVMController {
    
    typealias ViewModelType = Y
    
    var viewModel: Y?
    
    required convenience init(viewModel: Y) {
        self.init()
        
        self.viewModel = viewModel
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    typealias ViewType = T
    
    var baseView: T? {
        return view as? T
    }
    
    override func loadView() {
        super.loadView()
        view = T()
    }
}
