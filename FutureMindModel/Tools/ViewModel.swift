//
//  ViewModel.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation

protocol ViewModel: class {
    associatedtype Coordinator: FlowCoordinator
    
    var flowCoordinator: Coordinator? { get }
    
    init(flowCoordinator: Coordinator)
}
