//
//  FlowCoordinator.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit

protocol FlowCoordinator: class {
    associatedtype Navigator: UIViewController
    
    var navigator: Navigator? { get set }
    
    init()
    
    static func prepareView(with navigator: Navigator?) -> UIViewController
}
