//
//  MVVMController.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation
import UIKit

protocol MVVMController: class {
    
    associatedtype ViewModelType: ViewModel    
    
    var viewModel: ViewModelType? { get set }
    
    init(viewModel: ViewModelType)
}
