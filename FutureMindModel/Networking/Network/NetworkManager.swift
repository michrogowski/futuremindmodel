//
//  NetworkManager.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation

class NetworkManager {
    
    init() { }
    
    static let timeout: TimeInterval = 60

    private lazy var operationQueue: OperationQueue = {
        
        var queue = OperationQueue()
        queue.name = "com.microgow.common.network.queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    @inline(__always) private func sessionConfiguration() -> URLSessionConfiguration {
        
        let config = URLSessionConfiguration.default
        
        config.httpAdditionalHeaders = ["Accept" : "application/vnd.github.mercy-preview+json"]
        
        config.timeoutIntervalForRequest = NetworkManager.timeout
        
        config.timeoutIntervalForResource = NetworkManager.timeout
        
        config.httpMaximumConnectionsPerHost = 5
        
        config.urlCache = URLCache(memoryCapacity: 210000000, diskCapacity: 1000000000, diskPath: nil)
        
        config.requestCachePolicy = .useProtocolCachePolicy
        
        return config
    }
    
    lazy var session: URLSession = {
        return URLSession(configuration: self.sessionConfiguration(), delegate: nil, delegateQueue: operationQueue)
    }()
    
    func send(query: NetworkQuery, completion: @escaping (NetworkResult)->()) {
        let requestTask = NetworkTask(query: query)
        
        let task = session.dataTask(with: requestTask as URLRequest) { (data, response, error) in
            DispatchQueue.main.async {
                completion(NetworkResult(data: data, urlResponse: response, error: error))
            }
        }
        
        query.task = task
        task.resume()
    }
    
    func cancel(query: NetworkQuery?) {
        query?.task?.cancel()
    }
    
    func suspend(query: NetworkQuery?) {
        query?.task?.suspend()
    }
    
    func resume(query: NetworkQuery?) {
        query?.task?.resume()
    }
}
