//
//  NetworkResult.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation

class NetworkResult {
    
    /// Response data.
    let data: Data?
    
    /// Response error, set if query failed.
    let error: Error?
    
    /// Network response for request
    let urlResponse: URLResponse?
    
    /// URLResponse headers
    var headers: [String: Any]? {
        return (urlResponse as? HTTPURLResponse)?.allHeaderFields as? [String: Any]
    }
    
    /// Result as Codable
    func result<T>(_ type: T.Type, decoder: JSONDecoder = JSONDecoder()) throws -> T where T : Codable {
        guard let responseData = data else { throw NetworkError(data: data, urlRespone: urlResponse) }
        
        do {
            return try decoder.decode(T.self, from: responseData)
        } catch let error as NSError {
            throw error
        }
    }
    
    init(data aData: Data?, urlResponse response: URLResponse?, error aError: Error?) {
        data = aData
        urlResponse = response
        error = aError
    }    
}
