//
//  NetworkQuery.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

public protocol NetworkQuery: class {
    /// HTTP URL.
    var url: URL { get }
    
    /// Query method.
    var method: HTTPMethod { get }
    
    /// Query parameters,
    var parametersData: Data? { get }
    
    /// Task that is connected with the query
    var task: URLSessionDataTask? { set get }
    
    /// Cache policy for this task
    var cachePolicy: NSURLRequest.CachePolicy { get }
    
}


public extension NetworkQuery {
    
    var parametersData: Data? { return nil }
    
    var method: HTTPMethod { return .get }
    
    var cachePolicy: NSURLRequest.CachePolicy { return .useProtocolCachePolicy }
}
