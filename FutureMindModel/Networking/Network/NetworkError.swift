//
//  NetworkError.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation

public class NetworkError: Error {
    let data: Data?
    let urlRespone: URLResponse?
    
    init(data aData: Data?, urlRespone response: URLResponse?) {
        data = aData
        urlRespone = response
    }
    
    public var localizedDescription: String {
        return "Data response is nil for network url \(String(describing: urlRespone?.url))"
    }
}
