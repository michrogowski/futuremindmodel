//
//  NetworkTask.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import Foundation

class NetworkTask: NSMutableURLRequest {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("aDecoder is not initialized for NetworkTask")
    }
    
    init(query: NetworkQuery) {
        
        super.init(url: query.url, cachePolicy: query.cachePolicy, timeoutInterval: NetworkManager.timeout)
        
        addValue("application/json", forHTTPHeaderField: "Content-Type")
        addValue("gzip", forHTTPHeaderField: "Content-Encoding")
        
        httpMethod = query.method.rawValue

        url = query.url

        if query.method != .get {
            httpBody = query.parametersData
        } else if let data = query.parametersData {
            setupGetParameters(for: data)
        }
    }
    
    private func setupGetParameters(for data: Data) {
        guard let dataString = String(data: data, encoding: .utf8), let getUrl = url else { return }
        
        var urlComponents = URLComponents(url: getUrl, resolvingAgainstBaseURL: false)
        
        guard let dictionary = convertToDictionary(text: dataString) else { return }
        
        urlComponents?.queryItems = dictionary.compactMap {
            return URLQueryItem(name: $0.key, value: $0.value as? String ?? String(describing: $0.value))
        }
        
        url = urlComponents?.url
    }
    
    private func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
            } catch {
                assertionFailure(error.localizedDescription)
            }
        }
        return nil
    }
}
