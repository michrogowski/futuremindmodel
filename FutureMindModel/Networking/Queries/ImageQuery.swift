//
//  ImageQuery.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 30/01/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit

public class ImageQuery: NetworkQuery {
    public var url: URL {
        guard let requestUrl = URL(string: stringUrl) else {
            return URL(fileURLWithPath: "")
        }
        
        return requestUrl
    }
    
    public var parametersData: Data?
    
    public weak var task: URLSessionDataTask?
    
    private let stringUrl: String
    
    public var cachePolicy: NSURLRequest.CachePolicy {
        return .returnCacheDataElseLoad
    }
    
    public init(url: String) {
        stringUrl = url
    }
}
