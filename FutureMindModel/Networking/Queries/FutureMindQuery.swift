//
//  FutureMindQuery.swift
//  FutureMindModel
//
//  Created by Michał Rogowski on 01/02/2019.
//  Copyright © 2019 Michał Rogowski. All rights reserved.
//

import UIKit

final class FutureMindQuery: NetworkQuery {
    var url: URL {
        return BaseConfiguration.baseURL.appendingPathComponent("recruitment-task")
    }
    
    weak var task: URLSessionDataTask?
    
    var cachePolicy: NSURLRequest.CachePolicy {
        return cache
    }
    
    private let cache: NSURLRequest.CachePolicy
    
    init(cachePolicy: NSURLRequest.CachePolicy = .useProtocolCachePolicy) {
        cache = cachePolicy
    }
}
